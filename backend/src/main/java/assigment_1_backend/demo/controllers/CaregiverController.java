package assigment_1_backend.demo.controllers;

import assigment_1_backend.demo.dtos.validator.CaregiverDTO;
import assigment_1_backend.demo.services.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value="/caregiver")

public class CaregiverController {

    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @GetMapping()
    public ResponseEntity<List<CaregiverDTO>> getCaregiver() {
        List<CaregiverDTO> dtos = caregiverService.findCaregiver();
        for (CaregiverDTO dto : dtos) {
            Link personLink = linkTo(methodOn(CaregiverController.class)
                    .getCaregiverbyID(dto.getId())).withRel("caregiverDetails");
            dto.add(personLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertCaregiver(@Valid @RequestBody CaregiverDTO caregiverDTO) {
        UUID caregiverID = caregiverService.insert(caregiverDTO);
        return new ResponseEntity<>(caregiverID, HttpStatus.CREATED);
    }


    @GetMapping(value = "caregiver/{id}")
    public ResponseEntity<CaregiverDTO> getCaregiverbyID(@PathVariable("id") UUID caregiverID) {
        CaregiverDTO dto = caregiverService.findCaregiverById(caregiverID);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }



    @DeleteMapping(value = "/{id}")
    public ResponseEntity<UUID> deleteByID(@PathVariable("id") UUID caregiverID){
        caregiverService.deleteCaregiverById(caregiverID);
        UUID personID=caregiverID;
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }


    @DeleteMapping(value = "/{name}")
    public ResponseEntity<String> deleteByName(@PathVariable("name") String name){
        caregiverService.deleteCaregiverByName(name);
        String personNAme=name;
        return new ResponseEntity<>(personNAme, HttpStatus.CREATED);
    }




}
