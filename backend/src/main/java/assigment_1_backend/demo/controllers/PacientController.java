package assigment_1_backend.demo.controllers;


import assigment_1_backend.demo.dtos.validator.PacientDTO;
import assigment_1_backend.demo.entities.Pacient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import assigment_1_backend.demo.services.PacientService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/pacient")
public class PacientController {

    private final PacientService pacientService;

    @Autowired
    public PacientController(PacientService pacientService) {
        this.pacientService = pacientService;
    }

    @GetMapping()
    public ResponseEntity<List<PacientDTO>> getPacient() {
        List<PacientDTO> dtos = pacientService.findPacient();
        for (PacientDTO dto : dtos) {
            Link personLink = linkTo(methodOn(PacientController.class)
                    .getPacient(dto.getId())).withRel("personDetails");
            dto.add(personLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertPacient(@Valid @RequestBody PacientDTO pacientDTO) {
        UUID personID = pacientService.insert(pacientDTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }


    @GetMapping(value = "pacient/{id}")
    public ResponseEntity<PacientDTO> getPacient(@PathVariable("id") UUID pacientID) {
        PacientDTO dto = pacientService.findPacientById(pacientID);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

  /* @DeleteMapping(value = "/{id}")
    public ResponseEntity<UUID> deleteByID(@PathVariable("id") UUID pacientID){
        pacientService.deletePacientById(pacientID);
        UUID personID=pacientID;
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }*/

    @DeleteMapping(value = "/{name}")
    public ResponseEntity<String> deleteByname(@PathVariable("name") String pacientname){
        pacientService.deletePacientByname(pacientname);
        String personID=pacientname;
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }
}
