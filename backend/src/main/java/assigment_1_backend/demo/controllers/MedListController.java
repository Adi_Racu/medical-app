package assigment_1_backend.demo.controllers;

import assigment_1_backend.demo.dtos.validator.MedListDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import assigment_1_backend.demo.services.MedListService;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/MedList")
public class MedListController {

    private final MedListService MedListService;

    @Autowired
    public MedListController(MedListService MedListService) {
        this.MedListService = MedListService;
    }

    @GetMapping()
    public ResponseEntity<List<MedListDTO>> getMedList() {
        List<MedListDTO> dtos = MedListService.findMedList();
        for (MedListDTO dto : dtos) {
            Link personLink = linkTo(methodOn(MedListController.class)
                    .getMedList(dto.getId())).withRel("personDetails");
            dto.add(personLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertMedList(@Valid @RequestBody MedListDTO MedListDTO) {
        UUID personID = MedListService.insert(MedListDTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }


    @GetMapping(value = "MedList/{id}")
    public ResponseEntity<MedListDTO> getMedList(@PathVariable("id") UUID MedListID) {
        MedListDTO dto = MedListService.findMedListById(MedListID);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

  /* @DeleteMapping(value = "/{id}")
    public ResponseEntity<UUID> deleteByID(@PathVariable("id") UUID MedListID){
        MedListService.deleteMedListById(MedListID);
        UUID personID=MedListID;
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }*/

    @DeleteMapping(value = "/{name}")
    public ResponseEntity<String> deleteByname(@PathVariable("name") String MedListname){
        MedListService.deleteMedListByName(MedListname);
        String personID=MedListname;
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }
}
