package assigment_1_backend.demo.controllers;

import assigment_1_backend.demo.dtos.validator.MedicineDTO;
import assigment_1_backend.demo.services.MedicineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value="/medicine")


public class MedicineController {

    private final MedicineService medicineService;

    @Autowired
    public MedicineController(MedicineService medicineService) {
        this.medicineService = medicineService;
    }

    @GetMapping()
    public ResponseEntity<List<MedicineDTO>> getMedicine() {
        List<MedicineDTO> dtos = medicineService.findMedicine();
        for (MedicineDTO dto : dtos) {
            Link personLink = linkTo(methodOn(MedicineController.class)
                    .getMedicinebyID(dto.getId())).withRel("personDetails");
            dto.add(personLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertMedicine(@Valid @RequestBody MedicineDTO medicineDTO) {
        UUID medicineID = medicineService.insert(medicineDTO);
        return new ResponseEntity<>(medicineID, HttpStatus.CREATED);
    }


    @GetMapping(value = "pacient/{id}")
    public ResponseEntity<MedicineDTO> getMedicinebyID(@PathVariable("id") UUID medicineID) {
        MedicineDTO dto = medicineService.findMedicineById(medicineID);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<UUID> deleteByID(@PathVariable("id") UUID medicineID){
        medicineService.deleteMedicineById(medicineID);
        UUID responseID=medicineID;
        return new ResponseEntity<>(responseID, HttpStatus.CREATED);
    }




}
