package assigment_1_backend.demo.entities;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.UUID;



@Entity
public class Medicine implements  Serializable{
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name = "med_name", nullable = false)
    private String med_name;

    @Column(name = "adverse_efects", nullable = false)
    private String adverse_efects;

    @Column(name = "dosage", nullable = false)
    private double dosage;


    public Medicine() {
    }

    public Medicine(UUID id, String med_name, String adverse_efects, double dosage) {
        this.id = id;
        this.med_name = med_name;
        this.adverse_efects = adverse_efects;
        this.dosage = dosage;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getmed_name() {
        return med_name;
    }

    public void setmed_name(String med_name) {
        this.med_name = med_name;
    }

    public String getadverse_efects() {
        return adverse_efects;
    }

    public void setadverse_efects(String adverse_efects) {
        this.adverse_efects = adverse_efects;
    }

    public double getdosage() {
        return dosage;
    }

    public void setdosage(int dosage) {
        this.dosage = dosage;
    }




}
