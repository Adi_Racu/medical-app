package assigment_1_backend.demo.entities;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.UUID;


@Entity
public class Pacient implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "birth_date", nullable = false)
    private int birth_date;

    @Column(name = "gender", nullable = false)
    private String gender;

    @Column(name = "age", nullable = false)
    private int age;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "id_caregiver", nullable = false)
    private int id_caregiver;

    @Column(name = "medical_condition", nullable = false)
    private String medical_condition;

    public Pacient() {
    }

    public Pacient(UUID id, String name, int birth_date, String gender, int age, String address, int id_caregiver, String medical_condition) {
        this.id = id;
        this.name = name;
        this.birth_date = birth_date;
        this.gender = gender;
        this.age = age;
        this.address = address;
        this.id_caregiver = id_caregiver;
        this.medical_condition = medical_condition;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getbirth_date() {
        return birth_date;
    }

    public void setbirth_date(int birth_date) {
        this.birth_date = birth_date;
    }

    public int getage() {
        return age;
    }

    public void setage(int age) {
        this.age = age;
    }

    public String getgender() { return gender; }

    public void setgender(String gender) {
        this.gender = gender;
    }

    public String getaddress() {
        return address;
    }

    public void setaddress(String address) {
        this.address = address;
    }

    public int getid_caregiver() {
        return id_caregiver;
    }

    public void setid_caregiver(int id_caregiver) {
        this.id_caregiver = id_caregiver;
    }

    public String getmedical_condition() {
        return medical_condition;
    }

    public void setmedical_condition(int id_caregiver) {
        this.medical_condition = medical_condition;
    }
}
