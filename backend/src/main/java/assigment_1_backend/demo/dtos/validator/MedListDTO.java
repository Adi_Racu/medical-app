package assigment_1_backend.demo.dtos.validator;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.Column;
import java.util.UUID;

public class MedListDTO extends RepresentationModel<MedListDTO> {

    private UUID id;
    private String med_name;
    private String pacient;
    private double dosage;

public MedListDTO(){

}

    public MedListDTO(UUID id, String med_name, String pacient, double dosage) {
        this.id = id;
        this.med_name = med_name;
        this.pacient = pacient;
        this.dosage = dosage;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getMed_name() {
        return med_name;
    }

    public void setMed_name(String med_name) {
        this.med_name = med_name;
    }

    public String getPacient() {
        return pacient;
    }

    public void setPacient(String pacient) {
        this.pacient = pacient;
    }

    public double getDosage() {
        return dosage;
    }

    public void setDosage(double dosage) {
        this.dosage = dosage;
    }
}
