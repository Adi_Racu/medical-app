package assigment_1_backend.demo.dtos.validator;
import org.springframework.hateoas.RepresentationModel;

import java.util.UUID;


public class PacientDTO extends RepresentationModel<PacientDTO>{
    private UUID id;
    private String name;
    private int age;
    private int birth_date;
    private String gender;
    private String address;
    private int id_caregiver;
    private String medical_condition;


public PacientDTO(){
}

public PacientDTO(UUID id,String name,int birth_date,String gender,int age,String adress,int id_caregiver,String medical_condition ){
     this.id=id;
     this.name=name;
     this.birth_date=birth_date;
     this.gender=gender;
     this.age=age;
     this.address=address;
     this.id_caregiver=id_caregiver;
     this.medical_condition=medical_condition;
    }
public UUID getId(){return id;}

public void setId(UUID id){
    this.id=id;
}

public String getName(){ return name;}

public void setName(String name){
    this.name=name;
}

public int getBirth_date(){return birth_date;}

public void setBirth_date(int birth_date){
    this.birth_date=birth_date;
}

public String getGender(){return gender;}

public void setGender(String gender){
    this.gender=gender;
}

public int getAge() { return age;}

public void setAge(int age) {
    this.age = age;
}

public String getAdress() { return address;}

public void setAddress(String address) {
    this.address = address;
}

public int getId_caregiver() {return id_caregiver;}

public void setId_caregiver(int id_caregiver) {
    this.id_caregiver = id_caregiver;
}

public String getMedical_condition() {return medical_condition; }

public void setMedical_condition(String medical_condition) {
    this.medical_condition = medical_condition;
}



}
