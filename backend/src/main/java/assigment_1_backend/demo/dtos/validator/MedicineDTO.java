package assigment_1_backend.demo.dtos.validator;
import org.springframework.hateoas.RepresentationModel;

import java.util.UUID;


public class MedicineDTO extends RepresentationModel<PacientDTO>{
    private UUID id;
    private String med_name;
    private String adverse_efects;
    private double dosage;

public MedicineDTO(){

}

public MedicineDTO(UUID id, String med_name, String adverse_efects, double dosage) {
    this.id = id;
    this.med_name = med_name;
    this.adverse_efects = adverse_efects;
    this.dosage = dosage;
}

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getMed_name() {
        return med_name;
    }

    public void setMed_name(String med_name) {
        this.med_name = med_name;
    }

    public String getAdverse_efects() {
        return adverse_efects;
    }

    public void setAdverse_efects(String adverse_efects) {
        this.adverse_efects = adverse_efects;
    }

    public double getDosage() {
        return dosage;
    }

    public void setDosage(double dosage) {
        this.dosage = dosage;
    }

}
