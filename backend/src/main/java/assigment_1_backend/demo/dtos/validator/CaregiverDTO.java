package assigment_1_backend.demo.dtos.validator;
import org.springframework.hateoas.RepresentationModel;

import java.util.UUID;

public class CaregiverDTO  extends RepresentationModel<CaregiverDTO>{
    private UUID id;
    private String name;

public CaregiverDTO(){}

    public CaregiverDTO(UUID id, String name) {
        this.id = id;
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
