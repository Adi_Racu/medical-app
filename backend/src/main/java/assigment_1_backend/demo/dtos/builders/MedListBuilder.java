package assigment_1_backend.demo.dtos.builders;

import assigment_1_backend.demo.dtos.validator.MedListDTO;
import assigment_1_backend.demo.entities.MedList;


public class MedListBuilder {

  private MedListBuilder(){}
    public static MedListDTO toMedListDTO(MedList medList){
        return new MedListDTO(medList.getId(), medList.getMed_name(), medList.getPacient(), medList.getDosage());
    }

    public static MedList toEntity(MedListDTO medListDTO){
        return new MedList(medListDTO.getId(), medListDTO.getMed_name(), medListDTO.getPacient(), medListDTO.getDosage());
    }


}
