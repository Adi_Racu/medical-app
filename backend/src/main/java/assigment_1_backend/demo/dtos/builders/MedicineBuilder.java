package assigment_1_backend.demo.dtos.builders;


import assigment_1_backend.demo.dtos.validator.MedicineDTO;
import assigment_1_backend.demo.entities.Medicine;

public class MedicineBuilder {

    private MedicineBuilder(){
    }

    public static MedicineDTO medicineDTO(Medicine medicine){
        return new MedicineDTO(medicine.getId(),medicine.getmed_name(),medicine.getadverse_efects(),medicine.getdosage());
    }

   public static Medicine toEntity(MedicineDTO medicineDTO){
        return new Medicine(medicineDTO.getId(),medicineDTO.getMed_name(),medicineDTO.getAdverse_efects(),medicineDTO.getDosage());
   }


}
