package assigment_1_backend.demo.dtos.builders;

import assigment_1_backend.demo.dtos.validator.CaregiverDTO;

import assigment_1_backend.demo.entities.Caregiver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import assigment_1_backend.demo.services.PacientService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

public class CaregiverBuilder {

    private CaregiverBuilder(){

    }

    public static CaregiverDTO toCaregiverDTO(Caregiver caregiver){
        return new CaregiverDTO(caregiver.getId(),caregiver.getName());
    }

    public static Caregiver toEntity(CaregiverDTO caregiverDTO){
        return new Caregiver(caregiverDTO.getId(),caregiverDTO.getName());
    }

}
