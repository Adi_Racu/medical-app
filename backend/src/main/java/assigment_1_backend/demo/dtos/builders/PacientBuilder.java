package assigment_1_backend.demo.dtos.builders;


import assigment_1_backend.demo.dtos.validator.PacientDTO;
import assigment_1_backend.demo.entities.Pacient;

public class PacientBuilder {

    private  PacientBuilder(){
    }

    public static PacientDTO toPacientDTO(Pacient pacient){
        return new PacientDTO(pacient.getId(),pacient.getName(), pacient.getbirth_date(),pacient.getgender(),pacient.getage(),pacient.getaddress(),pacient.getid_caregiver(),pacient.getmedical_condition());
    }

    public static Pacient toEntity(PacientDTO pacientDTO){
        return new Pacient(pacientDTO.getId(),pacientDTO.getName(), pacientDTO.getBirth_date(),pacientDTO.getGender(),pacientDTO.getAge(),pacientDTO.getAdress(),pacientDTO.getId_caregiver(),pacientDTO.getMedical_condition());
    }

}
