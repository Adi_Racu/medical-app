package assigment_1_backend.demo.services;

import assigment_1_backend.demo.dtos.builders.CaregiverBuilder;
import assigment_1_backend.demo.dtos.validator.CaregiverDTO;
import assigment_1_backend.demo.entities.Caregiver;

import assigment_1_backend.demo.repositories.CaregiverRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CaregiverService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CaregiverService.class);
    private final CaregiverRepository caregiverRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository){

        this.caregiverRepository = caregiverRepository;
    }

    public List<CaregiverDTO> findCaregiver() {
        List<Caregiver> caregiveList = caregiverRepository.findAll();
        return caregiveList.stream()
                .map(CaregiverBuilder::toCaregiverDTO)
                .collect(Collectors.toList());
    }
    public CaregiverDTO findCaregiverById(UUID id) {
        Optional<Caregiver> prosumerOptional = caregiverRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("caregiver with id {} was not found in db", id);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + id);
        }
        return CaregiverBuilder.toCaregiverDTO(prosumerOptional.get());
    }

    public UUID insert(CaregiverDTO medicineDTO) {
        Caregiver caregiver = CaregiverBuilder.toEntity(medicineDTO);
        caregiver = caregiverRepository.save(caregiver);
        LOGGER.debug("Person with id {} was inserted in db", caregiver.getId());
        return caregiver.getId();
    }

    public void deleteCaregiverById(UUID id) {
        Optional<Caregiver> prosumerOptional = caregiverRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + id);
        }
        caregiverRepository.deleteById(id);
    }

    public void deleteCaregiverByName(String name) {
        Optional<Caregiver> prosumerOptional = caregiverRepository.getCaregiverByName(name);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", name);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with name: " + name);
        }
        caregiverRepository.deleteCaregiverByName(name);
    }




}
