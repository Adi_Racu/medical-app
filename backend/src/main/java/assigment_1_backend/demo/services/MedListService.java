package assigment_1_backend.demo.services;

import assigment_1_backend.demo.dtos.builders.MedListBuilder;
import assigment_1_backend.demo.dtos.validator.MedListDTO;
import assigment_1_backend.demo.entities.MedList;
import assigment_1_backend.demo.repositories.MedListRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MedListService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MedListService.class);
    private final MedListRepository MedListRepository;

    @Autowired
    public MedListService(MedListRepository MedListRepository){

        this.MedListRepository = MedListRepository;
    }

    public  List<MedListDTO> findMedList() {
        List<MedList> caregiveList = MedListRepository.findAll();
        return caregiveList.stream()
                .map(MedListBuilder::toMedListDTO)
                .collect(Collectors.toList());
    }
    public MedListDTO findMedListById(UUID id) {
        Optional<MedList> prosumerOptional = MedListRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("MedList with id {} was not found in db", id);
            throw new ResourceNotFoundException(MedList.class.getSimpleName() + " with id: " + id);
        }
        return MedListBuilder.toMedListDTO(prosumerOptional.get());
    }

    public UUID insert(MedListDTO medicineDTO) {
        MedList MedList = MedListBuilder.toEntity(medicineDTO);
        MedList = MedListRepository.save(MedList);
        LOGGER.debug("Person with id {} was inserted in db", MedList.getId());
        return MedList.getId();
    }

    public void deleteMedListById(UUID id) {
        Optional<MedList> prosumerOptional = MedListRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(MedList.class.getSimpleName() + " with id: " + id);
        }
        MedListRepository.deleteById(id);
    }

    public void deleteMedListByName(String name) {
        Optional<MedList> prosumerOptional = MedListRepository.findMedListBypacient(name);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", name);
            throw new ResourceNotFoundException(MedList.class.getSimpleName() + " with name: " + name);
        }
        MedListRepository.deleteMedListBypacient(name);
    }





}
