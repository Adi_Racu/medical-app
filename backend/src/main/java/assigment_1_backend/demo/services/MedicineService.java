package assigment_1_backend.demo.services;

import assigment_1_backend.demo.dtos.builders.MedicineBuilder;
import assigment_1_backend.demo.dtos.builders.PacientBuilder;
import assigment_1_backend.demo.dtos.validator.MedicineDTO;
import assigment_1_backend.demo.dtos.validator.PacientDTO;
import assigment_1_backend.demo.entities.Medicine;
import assigment_1_backend.demo.entities.Pacient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import assigment_1_backend.demo.repositories.MedicineRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class MedicineService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MedicineService.class);
    private final MedicineRepository medicinetRepository;

    @Autowired
    public MedicineService(MedicineRepository medicinetRepository){

        this.medicinetRepository = medicinetRepository;
    }

    public List<MedicineDTO> findMedicine() {
        List<Medicine> medicineList = medicinetRepository.findAll();
        return medicineList.stream()
                .map(MedicineBuilder::medicineDTO)
                .collect(Collectors.toList());
    }
    public MedicineDTO findMedicineById(UUID id) {
        Optional<Medicine> prosumerOptional = medicinetRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Medicine with id {} was not found in db", id);
            throw new ResourceNotFoundException(Pacient.class.getSimpleName() + " with id: " + id);
        }
        return MedicineBuilder.medicineDTO(prosumerOptional.get());
    }

    public UUID insert(MedicineDTO medicineDTO) {
        Medicine medicine = MedicineBuilder.toEntity(medicineDTO);
        medicine = medicinetRepository.save(medicine);
        LOGGER.debug("Person with id {} was inserted in db", medicine.getId());
        return medicine.getId();
    }

    public void deleteMedicineById(UUID id) {
        Optional<Medicine> prosumerOptional = medicinetRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Medicine.class.getSimpleName() + " with id: " + id);
        }
        medicinetRepository.deleteById(id);
    }






}
