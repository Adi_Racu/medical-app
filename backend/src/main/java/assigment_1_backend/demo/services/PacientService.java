package assigment_1_backend.demo.services;


import assigment_1_backend.demo.dtos.builders.PacientBuilder;
import assigment_1_backend.demo.dtos.validator.PacientDTO;
import assigment_1_backend.demo.entities.Pacient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import assigment_1_backend.demo.repositories.PacientRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PacientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PacientService.class);
    private final PacientRepository pacientRepository;

    @Autowired
    public PacientService(PacientRepository pacientRepository){

        this.pacientRepository = pacientRepository;
    }

    public List<PacientDTO> findPacient() {
        List<Pacient> personList = pacientRepository.findAll();
        return personList.stream()
                .map(PacientBuilder::toPacientDTO)
                .collect(Collectors.toList());
    }
    public PacientDTO findPacientById(UUID id) {
        Optional<Pacient> prosumerOptional = pacientRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Pacient.class.getSimpleName() + " with id: " + id);
        }
        return PacientBuilder.toPacientDTO(prosumerOptional.get());
    }

    public UUID insert(PacientDTO pacientDTO) {
        Pacient pacient = PacientBuilder.toEntity(pacientDTO);
        pacient = pacientRepository.save(pacient);
        LOGGER.debug("Person with id {} was inserted in db", pacient.getId());
        return pacient.getId();
    }

    public void deletePacientById(UUID id) {
        Optional<Pacient> prosumerOptional = pacientRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Pacient.class.getSimpleName() + " with id: " + id);
        }
        pacientRepository.deleteById(id);
    }

    public void deletePacientByname(String name) {
        Optional<Pacient> prosumerOptional = pacientRepository.findPacientByName(name);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Pacient with id {} was not found in db", name);
            throw new ResourceNotFoundException(Pacient.class.getSimpleName() + " with id: " + name);
        }
        pacientRepository.deletePacientByName(name);
    }
}
