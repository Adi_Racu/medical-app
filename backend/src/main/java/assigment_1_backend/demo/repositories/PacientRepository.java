package assigment_1_backend.demo.repositories;

import assigment_1_backend.demo.entities.Pacient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PacientRepository extends  JpaRepository<Pacient,UUID> {
    @Transactional
    Optional<Pacient> findPacientByName(String name);

    @Transactional
    void deletePacientByName(String name);


}
