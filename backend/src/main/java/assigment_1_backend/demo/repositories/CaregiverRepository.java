package assigment_1_backend.demo.repositories;

import assigment_1_backend.demo.entities.Caregiver;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CaregiverRepository extends JpaRepository<Caregiver, UUID> {

@Transactional
 Optional<Caregiver> getCaregiverByName(String name);

@Transactional
void deleteCaregiverByName(String name);

}
