package assigment_1_backend.demo.repositories;

import assigment_1_backend.demo.entities.MedList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.Optional;
import java.util.UUID;

public interface MedListRepository extends JpaRepository<MedList, UUID> {
 @Transactional
    Optional<MedList>findMedListBypacient(String name);
 @Transactional
    void deleteMedListBypacient(String name);

}
