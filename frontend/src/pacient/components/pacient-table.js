import React from "react";
import Table from "../../commons/tables/table";


const columns = [
    {
        Header: 'Name',
        accessor: 'name',
    },
    {
        Header: 'Birth date',
        accessor: 'birth_date',
    },
    {
        Header: 'Gender',
        accessor: 'gender',
    },
    {
        Header: 'Age',
        accessor: 'age',
    },
    {
        Header: 'Address',
        accessor: 'address',
    },
    {
        Header: 'ID caregiver',
        accessor: 'id_caregiver',
    },
    {
        Header: 'Medical_condition',
        accessor: 'medical_condition',
    },

];

const filters = [
    {
        accessor: 'name',
    }
];

class PacientTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }
    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                search={filters}
                pageSize={5}
            />
        )
    }
}

export default PacientTable;