import React from 'react';
import validate from "./validators/pacient-validator";
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/pacient-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';



class PacientForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                name: {
                    value: '',
                    placeholder: 'What is your name?...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                birth_date: {
                    value: '',
                    placeholder: 'date of birth...',
                    valid: false,
                    touched: false,

                },
                gender: {
                    value: '',
                    placeholder: 'gender...',
                    valid: false,
                    touched: false,
                },
                age: {
                    value: '',
                    placeholder: 'age...',
                    valid: false,
                    touched: false,
                },
                address: {
                    value: '',
                    placeholder: 'Cluj, Zorilor, Str. Lalelelor 21...',
                    valid: false,
                    touched: false,
                },
                id_caregiver: {
                    value: '',
                    placeholder: 'id caregiver.',
                    valid: false,
                    touched: false,
                },
                medical_condition: {
                    value: '',
                    placeholder: 'boala...',
                    valid: false,
                    touched: false,
                },

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    registerPacient(pacient) {
        return API_USERS.postPacient(pacient, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted pacient with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        let pacient = {
            name: this.state.formControls.name.value,
            birth_date: this.state.formControls.birth_date.value,
            gender:this.state.formControls.gender.value,
            age:this.state.formControls.age.value,
            address: this.state.formControls.address.value,
            id_caregiver: this.state.formControls.id_caregiver.value,
            medical_condition:this.state.formControls.medical_condition.value

        };

        console.log(pacient);
        this.registerPacient(pacient);
    }

    render() {
        return (
            <div>

                <FormGroup id='name'>
                    <Label for='nameField'> Name: </Label>
                    <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name.value}
                           touched={this.state.formControls.name.touched? 1 : 0}
                           valid={this.state.formControls.name.valid}
                           required
                    />
                    {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='birth_date'>
                    <Label for='birth_dateField'> birth_date: </Label>
                    <Input name='birth_date' id='birth_dateField' placeholder={this.state.formControls.birth_date.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.birth_date.value}
                           touched={this.state.formControls.birth_date.touched? 1 : 0}
                           valid={this.state.formControls.birth_date.valid}
                           required
                    />
                    {this.state.formControls.birth_date.touched && !this.state.formControls.birth_date.valid &&
                    <div className={"error-message"}> * Email must have a valid format</div>}
                </FormGroup>

                <FormGroup id='gender'>
                    <Label for='genderField'> gender: </Label>
                    <Input name='gender' id='genderField' placeholder={this.state.formControls.gender.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.gender.value}
                           touched={this.state.formControls.gender.touched? 1 : 0}
                           valid={this.state.formControls.gender.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='age'>
                    <Label for='ageField'> Age: </Label>
                    <Input name='age' id='ageField' placeholder={this.state.formControls.age.placeholder}
                           min={0} max={100} type="number"
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.age.value}
                           touched={this.state.formControls.age.touched? 1 : 0}
                           valid={this.state.formControls.age.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='address'>
                    <Label for='addressField'> address: </Label>
                    <Input name='address' id='addressField' placeholder={this.state.formControls.address.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.address.value}
                           touched={this.state.formControls.address.touched? 1 : 0}
                           valid={this.state.formControls.address.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='id_caregiver'>
                    <Label for='id_caregiver_Field'> id_caregiver: </Label>
                    <Input name='id_caregiver' id='id_caregiver_Field' placeholder={this.state.formControls.id_caregiver.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.id_caregiver.value}
                           touched={this.state.formControls.id_caregiver.touched? 1 : 0}
                           valid={this.state.formControls.id_caregiver.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='medical_condition'>
                    <Label for='medical_condition_Field'> medical_condition: </Label>
                    <Input name='medical_condition' id='medical_condition_Field' placeholder={this.state.formControls.medical_condition.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.medical_condition.value}
                           touched={this.state.formControls.medical_condition.touched? 1 : 0}
                           valid={this.state.formControls.medical_condition.valid}
                           required
                    />
                </FormGroup>


                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Submit </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }
}

export default PacientForm;
