import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';

import PacientContainer from './pacient/pacient-container'
import CaregiverContainer from './caregiver/caregiver-container'
import DoctorContainer from './doctor/doctor-container'
import PacientdeleteContainer from './pacient/delete/pacientdelete-container'
import MedListContainer from './medlist/medlist-container'
import MedListDeleteContainer from './medlist/delete/medlistdelete-container'


import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';

class App extends React.Component {


    render() {

        return (
            <div className={styles.back}>
            <Router>
                <div>
                    <NavigationBar />
                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <Home/>}
                        />


                        <Route
                            exact
                            path='/pacient'
                            render={() => <PacientContainer/>}
                        />

                        <Route
                            exact
                            path='/caregiver'
                            render={() => <CaregiverContainer/>}
                        />

                        <Route
                            exact
                            path='/doctor'
                            render={() => <DoctorContainer/>}
                        />

                        <Route
                            exact
                            path='/pacient/delete'
                            render={() => <PacientdeleteContainer/>}
                        />

                        <Route
                            exact
                            path='/medlist'
                            render={() => <MedListContainer/>}
                        />

                        <Route
                            exact
                            path='/medlist/delete'
                            render={() => <MedListDeleteContainer/>}
                        />

                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
