import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    medlist: '/MedList',
    deletemedlist: '/MedList/'
};

function getmedlist(callback) {
    let request = new Request(HOST.backend_api + endpoint.medlist , {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getmedlistById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.medlist + params.id, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postmedlist(user, callback){
    let request = new Request(HOST.backend_api + endpoint.medlist , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deletemedlistById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.medlist + params.id, {
        method: 'DELETE'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function deletemedlistByname(params, callback){
    let request = new Request(HOST.backend_api + endpoint.deletemedlist + params.name, {
        method: 'DELETE'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


export {
    getmedlist,
    getmedlistById,
    postmedlist,
    deletemedlistById,
    deletemedlistByname
};
