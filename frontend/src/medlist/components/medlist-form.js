import React from 'react';
import validate from "./validators/medlist-validators";
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/medlist-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';



class MedlistForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                dosage: {
                    value: '',
                    placeholder: 'What is your dosage?...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                name: {
                    value: '',
                    placeholder: 'What is your name?...',
                    valid: false,
                    touched: false,

                },
                pacient: {
                    value: '',
                    placeholder: 'What is your pacient?...',
                    valid: false,
                    touched: false,
                },


            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    registermedlist(medlist) {
        return API_USERS.postmedlist(medlist, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted medlist with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        let medlist = {
            dosage: this.state.formControls.name.value,
            name: this.state.formControls.birth_date.value,
            pacient:this.state.formControls.gender.value,


        };

        console.log(medlist);
        this.registermedlist(medlist);
    }

    render() {
        return (
            <div>

                <FormGroup id='dosage'>
                    <Label for='dosageField'> Name: </Label>
                    <Input name='dosage' id='dosageField' placeholder={this.state.formControls.name.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name.value}
                           touched={this.state.formControls.name.touched? 1 : 0}
                           valid={this.state.formControls.name.valid}
                           required
                    />
                    {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='name'>
                    <Label for='nameField'> name: </Label>
                    <Input name='name' id='nameField' placeholder={this.state.formControls.birth_date.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.birth_date.value}
                           touched={this.state.formControls.birth_date.touched? 1 : 0}
                           valid={this.state.formControls.birth_date.valid}
                           required
                    />
                    {this.state.formControls.birth_date.touched && !this.state.formControls.birth_date.valid &&
                    <div className={"error-message"}> * Email must have a valid format</div>}
                </FormGroup>

                <FormGroup id='pacient'>
                    <Label for='pacientField'> gender: </Label>
                    <Input name='pacient' id='pacientField' placeholder={this.state.formControls.gender.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.gender.value}
                           touched={this.state.formControls.gender.touched? 1 : 0}
                           valid={this.state.formControls.gender.valid}
                           required
                    />
                </FormGroup>




                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Submit </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }
}

export default MedlistForm;
