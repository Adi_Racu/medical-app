import React from "react";
import Table from "../../commons/tables/table";


const columns = [
    {
        Header: 'Dosage',
        accessor: 'dosage',
    },
    {
        Header: 'Medicine name',
        accessor: 'med_name',
    },
    {
        Header: 'Pacient',
        accessor: 'pacient',
    },



];

const filters = [
    {
        accessor: 'name',
    }
];

class MedlistTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }
    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                search={filters}
                pageSize={5}
            />
        )
    }
}
export  default MedlistTable;
