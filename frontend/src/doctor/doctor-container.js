import React from 'react';

import BackgroundImg from '../commons/images/future-medicine.jpg';

import {Button, Container, Jumbotron} from 'reactstrap';

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'white', };

class doctorContainer extends React.Component {


    render() {

        return (

            <div>
                <Jumbotron fluid style={backgroundStyle}>
                    <Container fluid>

                        <p className="lead">
                            <Button color="primary" onClick={() => window.open('http://localhost:3000/pacient')}>Pacients
                            </Button>
                        </p>
                        <p className="lead">
                            <Button color="primary" onClick={() => window.open('http://localhost:3000/caregiver')}>Caregiver
                            </Button>
                        </p>
                        <p className="lead">
                            <Button color="primary" onClick={() => window.open('http://localhost:3000/medication')}>Medication
                            </Button>
                        </p>
                        <p className="lead">
                            <Button color="primary" onClick={() => window.open('http://localhost:3000/medlist')}>MedicationList
                            </Button>
                        </p>

                    </Container>
                </Jumbotron>

            </div>
        )
    };
}

export default doctorContainer
